#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "assignment.h"

void testStringLength() 
{
    const char* testStrings[] = {
        "Hello, World",
        "This is a test string, doing some testing..."
    };

    for(int i = 0; i < 2; ++i) {
        printf("The length of '%s' is %d\n", testStrings[i], getStringLength(testStrings[i]));
    }
}

void testMemCopy()
{
    const int BUFSIZE = 1024;
    unsigned char* pSrcCharBuff = new unsigned char[BUFSIZE];
    unsigned char* pDstCharBuff = new unsigned char[BUFSIZE];
    int* pSrcIntBuff = new int[BUFSIZE];
    int* pDstIntBuff = new int[BUFSIZE];

    for(int i = 0; i < BUFSIZE; i++)
        pSrcCharBuff[i] = pDstCharBuff[i] = pSrcIntBuff[i] = pDstIntBuff[i] = 0;

    for(int i = 0; i < BUFSIZE; i++) 
    {
        pSrcCharBuff[i] = rand() % 255;
        pSrcIntBuff[i] = rand();
    }
    copyMemory(pSrcCharBuff, pDstCharBuff, BUFSIZE);
    copyMemory(pSrcIntBuff, pDstIntBuff, BUFSIZE*sizeof(int));

    for(int i = 0; i < BUFSIZE; i++) 
    {
        if(pSrcCharBuff[i] != pDstCharBuff[i] ||
           pSrcIntBuff[i] != pDstIntBuff[i]) 
        {
            printf("Memory copy failed at %d\n", i);
        }
    }

    delete[] pSrcCharBuff;
    delete[] pDstCharBuff;
    delete[] pSrcIntBuff;
    delete[] pDstIntBuff;
}

void testPlayerScore()
{
    const char* name = "A Clever Name";
    int len = strlen(name);
    char* buff = new char[len + sizeof(short) + sizeof(int) + 1];
    char* start = buff;

    *((int*)buff) = 12345; buff += 4;
    *((unsigned short*)buff) = (unsigned short)len; buff += 2;
    for(int i = 0; i <= len; ++i )
    {
        *buff = name[i];
        buff++;
    }

    printPlayerScore(start);
    delete[] start;
}

int main() 
{
    testStringLength();
    testMemCopy();
    testPlayerScore();
    return 0;
}